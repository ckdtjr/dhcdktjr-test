from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('join', views.join, name='join'),
    path('edit', views.edit, name='edit'),
    path('new', views.new, name='new'),
    path('no', views.no, name='no'),
    

]
