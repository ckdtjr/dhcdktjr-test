from django.urls import path
from . import views


urlpatterns = [
        path('search1', views.search1, name='search1'),
        path('list1', views.list1, name='list1'),
        path('test', views.test, name='test'),
        path('new', views.new, name='new'),
        path('service1', views.service1, name='service1'),
        path('service2', views.service2, name='service2'),
        path('service3', views.service3, name='service3'),
        path('service4', views.service4, name='service4'),

]
