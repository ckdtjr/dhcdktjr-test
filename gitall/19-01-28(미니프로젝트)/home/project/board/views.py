from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from.models import board1
from.models import Table1, Table2,Table3,Table4
from django.db.models import Sum, Max, Min, Count, Avg
import pandas as pd
import random
import matplotlib.pyplot as plt
import io
import base64
from matplotlib import font_manager, rc
# Create your views here.
# from.models import board1
cursor=connection.cursor()


@csrf_exempt
def new(request):
    return render(request, "board/new.html")

@csrf_exempt

def test(request):
     if request.method=='GET':
        w = request.GET.get('search', "")
        #주소get한게 없을때
        if w == "":
            li=[]
            for i in range(0,5000,1):
                NOs = str(random.randrange(1,900000))
                li.append(NOs)

            #Distinct : 중복제거 (데이터에 저장된 단어들 중복제거한거 보여주겠다. )
            sql="""
                    SELECT WORD FROM BOARD_BOARD1
                    WHERE NO = %s 
            """
            
            result=[]
            for i in li : 
                num = []
                num.append(i)
                cursor.execute(sql, num)
                data1=cursor.fetchone()
                result.append(data1)
            print(result)
            return render(request, "board/test.html",{"data":result})


def search1(request):
     if request.method=='GET':
        w = request.GET.get('search', "")
        #주소get한게 없을때
        if w == "":
            li=[]
            for i in range(0,50,1):
                NOs = str(random.randrange(1,900000))
                li.append(NOs)

            #Distinct : 중복제거 (데이터에 저장된 단어들 중복제거한거 보여주겠다. )
            sql="""
                    SELECT DISTINCT WORD FROM BOARD_BOARD1
                    WHERE NO = %s 
            """
            
            result=[]
            for i in li : 
                num = []
                num.append(i)
                cursor.execute(sql, num)
                data1=cursor.fetchone()
                result.append(data1)
                return render(request, "board/search1.html",{"data":result})
            
    


def list1(request):
    w=request.GET.get('search', "")
    print(w)
    return render(request, "board/list1.html")

@csrf_exempt
def service1(request):
    if request.method=='GET':
        w = request.GET.get('search', "")
        #주소get한게 없을때
        if w == "":
            li=[]
            for i in range(0,50,1):
                NOs = str(random.randrange(1,900000))
                li.append(NOs)

            #Distinct : 중복제거 (데이터에 저장된 단어들 중복제거한거 보여주겠다. )
            sql="""
                    SELECT DISTINCT WORD FROM BOARD_BOARD1
                    WHERE NO = %s 
            """
            
            result=[]
            for i in li : 
                num = []
                num.append(i)
                cursor.execute(sql, num)
                data1=cursor.fetchone()
                result.append(data1)

            dataw=list(Table1.objects.all())
            print("@@",dataw)
            return render(request, 'board/service1.html',{"data":result,"abs":dataw})            
        #주소 get 한게 있을 때 
        else:
            li=[]
            for i in range(0,50,1):
                NOs = str(random.randrange(1,900000))
                li.append(NOs)

            #Distinct : 중복제거 (데이터에 저장된 단어들 중복제거한거 보여주겠다. )
            sql="""
                    SELECT DISTINCT WORD FROM BOARD_BOARD1
                    WHERE NO = %s 
            """
            
            result=[]
            for i in li : 
                num = []
                num.append(i)
                cursor.execute(sql, num)
                data1=cursor.fetchone()
                result.append(data1)
            #처음 나온날 검색
            str1='%'+ w +'%'
            sql="""
                SELECT * FROM(
                    SELECT WORD, YEAR, MONTH, DAY, TIME, ROW_NUMBER() OVER (PARTITION BY WORD ORDER BY YEAR ASC, MONTH ASC, DAY ASC, TIME ASC) AS ROWN
                    FROM BOARD_BOARD1
                    WHERE WORD LIKE %s ) T1
                WHERE ROWN=1
            """            
            cursor.execute(sql, [str1])
            data1=cursor.fetchall()
            print(data1)
            #data1=data[0]
            #data2=data[len(data)-1]
            #print(data1)

            # 마지막으로 검색된 날
            sql="""
                SELECT* FROM(
                SELECT WORD, YEAR, MONTH, DAY, TIME, ROW_NUMBER() OVER (PARTITION BY WORD ORDER BY YEAR DESC, MONTH DESC, DAY DESC, TIME DESC) AS ROWN1
                FROM BOARD_BOARD1
                WHERE WORD LIKE %s) T2
                WHERE ROWN1=1
            """
            cursor.execute(sql,[str1])
            data2=cursor.fetchall()
            print(data2)
            li=[]
            for i in range(len(data1)):
                li.append((data1[i],data2[i]))

            dataw=Table1.objects.values("no","title","content","writer")

            return render(request, 'board/service1.html',{"li":li,"abs":dataw,"data":result})  #{{data.YEAR}}
    elif request.method=='POST':
    
        arr=[request.POST['title'],request.POST['content'],request.POST['writer']]
        print(arr)
        sql="""
            INSERT INTO BOARD_TABLE1(TITLE, CONTENT, WRITER)
            VALUES(%s, %s, %s)
        """
        cursor.execute(sql, arr)
        return redirect("/board/service1")

@csrf_exempt
def service2(request):
    if request.method=='GET':
        key =request.GET.get('search1', "")
        age =request.GET.get('age1', "")

        key3=request.GET.get('search2', "")
        age1=request.GET.get('age2', "")

        key4=request.GET.get('search3', "")
        age2=request.GET.get('age3', "")
        if key == "":###0개일때
            li=[]
            for i in range(0,10,1):
                NO1 = str(random.randrange(1,900000))
                li.append(NO1)

            sql="""
                    SELECT WORD FROM BOARD_BOARD1
                    WHERE NO = %s """
            result=[]
            for i in li : 
                num = []
                num.append(i)
                cursor.execute(sql, num)
                data1=cursor.fetchone()
                result.append(data1)
            print(result)
            # cursor.execute(sql, [li])
            # print(2)
            # data1=cursor.fetchone()
            # print(data1)
            dataw=Table2.objects.values("no","title","content","writer")

            return render(request, 'board/service2.html',{"data":result,"abs":dataw})            
        else:###1개일때
            if key3=="":
                sql="""
                    SELECT WORD, YEAR, MONTH, DAY, TIME, RANK, GENE FROM BOARD_BOARD1
                    WHERE WORD=%s AND GENE=%s
                    ORDER BY YEAR, MONTH, DAY, TIME ASC
                """

                cursor.execute(sql, [key,age])
                data=cursor.fetchall()

                data1 = []
                data2 = []
                for tmp in data:
                    data1.append( str(tmp[1])+"-"+str(tmp[2])+"-"+str(tmp[3])+"-"+str(tmp[4]) )
                    data2.append(21-tmp[5])
                
                print(data)
                #print(data1)
                # data1=data[0]
                # data2=data[len(data)-1]
                # print(data1)
                dataw=Table2.objects.values("no","title","content","writer")

                return render(request, 'board/service2.html',{"date":data, "data1":data1, "data2":data2, "key":key,"abs":dataw})  #{{data.YEAR}}
            else:### 2개일때
                if key3=="":
                    sql="""
                        SELECT WORD, YEAR, MONTH, DAY, TIME, RANK FROM BOARD_BOARD1
                        WHERE WORD=%s AND GENE=%s
                        ORDER BY YEAR, MONTH, DAY, TIME ASC
                    """

                    cursor.execute(sql, [key,age])
                    data=cursor.fetchall()

                    cursor.execute(sql, [key3,age1])
                    adddata=cursor.fetchall()
                    data1 = []
                    data2 = []
                    data3 = []
                    data4 = []
                    for tmp in data:
                        data1.append( str(tmp[1])+"-"+str(tmp[2])+"-"+str(tmp[3])+"-"+str(tmp[4]) )
                        data2.append(tmp[5])
                    for tmp in adddata:
                        data3.append( str(tmp[1])+"-"+str(tmp[2])+"-"+str(tmp[3])+"-"+str(tmp[4]) )
                        data4.append(tmp[5])
                    
                    print(data)
                    #print(data1)
                    # data1=data[0]
                    # data2=data[len(data)-1]
                    # print(data1)
                    dataw=Table2.objects.values("no","title","content","writer")
                    return render(request, 'board/service2.html',{"date":data, "data1":data1, "data2":data2, "data3":data3, "data4":data4, "key":key, "key1":key3,"abs":dataw})  #{{data.YEAR}}
                else:#3개일때
                    
                    sql="""
                        SELECT WORD, YEAR, MONTH, DAY, TIME, RANK FROM BOARD_BOARD1
                        WHERE WORD=%s AND GENE=%s
                        ORDER BY YEAR, MONTH, DAY, TIME ASC
                    """

                    cursor.execute(sql, [key,age])
                    data=cursor.fetchall()
                    cursor.execute(sql, [key3,age1])
                    adddata=cursor.fetchall()
                    cursor.execute(sql, [key4,age2])
                    add1data=cursor.fetchall()

                    data1 = []
                    data2 = []
                    data3 = []
                    data4 = []
                    data5 = []
                    data6 = []
                    for tmp in data:
                        data1.append( str(tmp[1])+"-"+str(tmp[2])+"-"+str(tmp[3])+"-"+str(tmp[4]) )
                        data2.append(tmp[5])
                    for tmp in adddata:
                        data3.append( str(tmp[1])+"-"+str(tmp[2])+"-"+str(tmp[3])+"-"+str(tmp[4]) )
                        data4.append(tmp[5])
                    for tmp in add1data:
                        data5.append( str(tmp[1])+"-"+str(tmp[2])+"-"+str(tmp[3])+"-"+str(tmp[4]) )
                        data6.append(tmp[5])
                        
                    
                    print(data)
                    #print(data1)
                    # data1=data[0]
                    # data2=data[len(data)-1]
                    # print(data1)
                    dataw=Table2.objects.values("no","title","content","writer")

                    return render(request, 'board/service2.html',{"date":data, "data1":data1, "data2":data2, "data3":data3, "data4":data4, "data5":data5, "data6":data6,"key":key, "key1":key3,"key2":key4,"abs":dataw})  #{{data.YEAR}}

    elif request.method=='POST':
        arr=[request.POST['title'],request.POST['content'],request.POST['writer']]
        print(arr)
        sql="""
            INSERT INTO BOARD_TABLE2(TITLE, CONTENT, WRITER)
            VALUES(%s, %s, %s)
        """
        cursor.execute(sql, arr)
        return redirect("/board/service2")
@csrf_exempt
def service3(request):
    if request.method == 'GET':
        # html에서 dropdown 메뉴에서 보내줄 get name값 리스트    
        # MM=['01','02','03','04','05','06','07','08','09']
        # for i in range(10, 13, 1):
        #     MM.append(str(i))

        # MM = [int(i) for i in range(1,13)]
        DD = [int(i) for i in range(1,32)]
        SS = [int(i) for i in range(0,24)]
        RK = [int(i) for i in range(1,21)]

        GN = ["10대","20대","30대","40대","50대이상"]
        a = list('0123')
        b = list('0123456789')
        serial= [i+j for i in a for j in b]
        MM = serial[1:13]
        # DD = serial[1:32]
        # SS = serial[:24]
    
    # 필터값 설정. 받아온 get name값 담기
        rk1  = int(request.GET.get('rank', 1))
        rk2  = int(request.GET.get('rank2', 1))

        gn1  = request.GET.get('gene', '10대') #10+20+30+40+50?
        gn1  = "%" +gn1+ "%"

        yy1  = str(request.GET.get('year', 2019))
        yy2  = str(request.GET.get('year2', 2020))
        mm1  = str(request.GET.get('month', "01"))
        mm2  = str(request.GET.get('month2', "01"))

        dd1  = str(request.GET.get('day', "1"))
        if len(dd1) < 2 :
            dd1 = "0" + dd1

        dd2  = str(request.GET.get('day2', "1"))
        if len(dd2) < 2  :
            dd2 = "0" + dd2

        ss1  = str(request.GET.get('time', "1"))
        if len(ss1) < 2  :
            ss1 = "0" + ss1

        ss2  = str(request.GET.get('time2', "1"))
        if len(ss2) < 2  :
            ss2 = "0" + ss2

        
        filters=[gn1,int(yy1+mm1+dd1+ss1),int(yy2+mm2+dd2+ss2),rk1,rk2]
   
    # 필터링 시작점
        sql = '''
        SELECT WORD,COUNT(*) FROM (
            SELECT WORD 
            FROM BOARD_BOARD1 
            WHERE 
                GENE LIKE %s AND
                TO_NUMBER(
                    YEAR||LPAD(MONTH,2,0)||LPAD(DAY,2,0)||LPAD(TIME,2,0)
                    ) >= %s AND
                TO_NUMBER(
                    YEAR||LPAD(MONTH,2,0)||LPAD(DAY,2,0)||LPAD(TIME,2,0)
                    ) <= %s AND
                RANK >= %s AND RANK <= %s
            )
        GROUP BY WORD
        ORDER BY COUNT(*) DESC
        '''
        cursor.execute(sql, filters)
        filtered = cursor.fetchall()
        print("=====================필터된 값=======",filtered)
        print("=====================필터1===========",filters)
    
    # 스크립트용 
        word1=list()
        ranking1=list()
        word2=list()
        ranking2=list()
        word3=list()
        ranking3=list()
        word4=list()
        ranking4=list()
        word5=list()
        ranking5=list()
        print()

        if(len(filtered)>=5):
            word1.append(filtered[0][0])
            ranking1.append(filtered[0][1])
            
            word2.append(filtered[1][0])
            ranking2.append(filtered[1][1])
            
            word3.append(filtered[2][0])
            ranking3.append(filtered[2][1])
            
            word4.append(filtered[3][0])
            ranking4.append(filtered[3][1])
            
            word5.append(filtered[4][0])
            ranking5.append(filtered[4][1])
            dataw=Table3.objects.values("no","title","content","writer")
            return render(request,'board/service3.html',{'GN':GN,'MM':MM,\
                "DD":DD,"SS":SS,"RK":RK,'filtered':filtered,\
                "word1":word1,"rank1":ranking1,"word2":word2,"rank2":ranking2,\
                "word3":word3,"rank3":ranking3,"word4":word4,"rank4":ranking4,\
                "word5":word5,"rank5":ranking5,"abs":dataw})
    
    # 그래프에 표시할 검색어가 5개 미만으로 필터링 되었을경우 그래프에 넣을 데이터 None
        else:
            dataw=Table3.objects.values("no","title","content","writer")
            return render(request,'board/service3.html',{'GN':GN,'MM':MM,\
            "DD":DD,"SS":SS,"RK":RK,'filtered':filtered,\
            "word1":None,"rank1":None,"word2":None,"rank2":None,\
            "word3":None,"rank3":None,"word4":None,"rank4":None,\
            "word5":None,"rank5":None,"abs":dataw})          

    # 댓글입력
    elif request.method=='POST':
        arr=[request.POST['title'],request.POST['content'],request.POST['writer']]
        print(arr)
        sql="""
            INSERT INTO BOARD_TABLE3(TITLE, CONTENT, WRITER)
            VALUES(%s, %s, %s)
        """
        cursor.execute(sql, arr)
        return redirect("/board/service3")




@csrf_exempt
def service4(request):
    if request.method=='GET':
        w=request.GET.get("ye", "")
        if w=="":
            li=[]
            for i in range(0,50,1):
                NO1 = str(random.randrange(1,900000))
                li.append(NO1)

            sql="""
                    SELECT WORD FROM BOARD_BOARD1
                    WHERE NO = %s """
            data=[]
            for i in li : 
                num = []
                num.append(i)
                cursor.execute(sql, num)
                data1=cursor.fetchone()
                data.append(data1)
            
            YEAR = [2020, 2019]
            MONTH = list(range(1,13,1))
            DAY = list(range(1,32,1))
            TIME = list(range(0,24,1))
            dataw=list(Table4.objects.all())
            return render(request, 'board/service4.html',{"li":data,"YE":YEAR, "MO":MONTH, "DA":DAY, "TI":TIME, "abs":dataw})
        else:
            w1=request.GET.get("ye", "")
            w2=request.GET.get("mo", "")
            w3=request.GET.get("da", "")
            w4=request.GET.get("ti", "")
            print(type(w1))
            
            ##연령대로 그룹만들어서 보여지기,순서 : 10, 20, 30, 40, 50 
            
            age10 = "%"+'10'+"%"
            age20 = "%"+'20'+"%"
            age30 = "%"+'30'+"%"
            age40 = "%"+'40'+"%"
            age50 = "%"+'50'+"%"
            
            sql1="""
                SELECT RANK, WORD, TIME FROM BOARD_BOARD1
                WHERE YEAR=%s AND MONTH=%s AND DAY=%s AND TIME=%s AND GENE LIKE %s
                ORDER BY GENE, RANK
            """
            sql2="""
                SELECT RANK, WORD, TIME FROM BOARD_BOARD1
                WHERE YEAR=%s AND MONTH=%s AND DAY=%s AND TIME=%s AND GENE LIKE %s
                ORDER BY GENE, RANK
            """
            sql3="""
                SELECT RANK, WORD, TIME FROM BOARD_BOARD1
                WHERE YEAR=%s AND MONTH=%s AND DAY=%s AND TIME=%s AND GENE LIKE %s
                ORDER BY GENE, RANK
            """
            sql4="""
                SELECT RANK, WORD, TIME FROM BOARD_BOARD1
                WHERE YEAR=%s AND MONTH=%s AND DAY=%s AND TIME=%s AND GENE LIKE %s
                ORDER BY GENE, RANK
            """
            sql5="""
                SELECT RANK, WORD, TIME FROM BOARD_BOARD1
                WHERE YEAR=%s AND MONTH=%s AND DAY=%s AND TIME=%s AND GENE LIKE %s
                ORDER BY GENE, RANK
            """
            arr10=[w1,w2,w3,w4, age10]
            cursor.execute(sql1, arr10)
            data1=cursor.fetchall()
            
            arr20=[w1,w2,w3,w4, age20]
            cursor.execute(sql2, arr20)
            data2=cursor.fetchall()

            arr30=[w1,w2,w3,w4, age30]
            cursor.execute(sql3, arr30)
            data3=cursor.fetchall()

            arr40=[w1,w2,w3,w4, age40]
            cursor.execute(sql4, arr40)
            data4=cursor.fetchall()

            arr50=[w1,w2,w3,w4, age50]
            cursor.execute(sql5, arr50)
            data5=cursor.fetchall()

            print(data1)
            print(data2)
            li=list()
            for i in range(len(data1)):
                li.append([data1[i],data2[i],data3[i],data4[i],data5[i]])
            print(li)
            dataw=list(Table4.objects.all())
            return render(request, 'board/service4.html',{"kk":li,"abs":dataw})

    elif request.method=='POST':
        arr=[request.POST['title'],request.POST['content'],request.POST['writer']]
        print(arr)
        sql="""
            INSERT INTO BOARD_TABLE4(TITLE, CONTENT, WRITER)
            VALUES(%s, %s, %s)
        """
        cursor.execute(sql, arr)
        return redirect("/board/service4")
    # elif request.method== 'POST':
    #     # HTML에서 넘어온 값 받기    
    #     ar = [request.POST['id']

    #     sql= "SELECT * FROM BOARD_BOARD1 WHERE ID=%s AND PW=%s"

    #     cursor.execute(sql, ar)
       
    #     search=cursor.fetchone()#한줄 가져 오기 아이디가 한개니깐 중복 되면 데이터 베이스가 엉망이란 소리 이므로.
                
    #     if data:
    #         request.session['search']=search[0]
            
    #         return redirect('/board/service2')
        

    #     return redirect('/board/index')

# @csrf_exempt
# def service2(request):
#     rank = list(board1.objects.values('rank')

#     str = '100, 200, 300, 100, 300, 200, 100'
#     return render(request, 'board/service2.html', {'str':str})
#     sum_eng= list(board1.objects.values('classroom').annotate(sum_eng=Sum('eng')))
#     print('1', sum_eng, type(sum_eng))

#     x=[sum_eng[0]['classroom'],sum_eng[1]['classroom'],sum_eng[2]['classroom'],sum_eng[3]['classroom']]
#     y=[sum_eng[0]['sum_eng'],sum_eng[1]['sum_eng'],sum_eng[2]['sum_eng'],sum_eng[3]['sum_eng']]
#     #폰트 읽기
#     font_name = font_manager.FontProperties(fname='c:/windows/fonts/malgun.ttf').get_name()
#     #폰트 적용
#     rc('font', family=font_name)

#     plt.bar(x,y)
#     plt.title('ages&person')
#     plt.xlabel('반')
#     plt.ylabel('영어합계점수')


#     plt.draw()#안보이게 그림을 캡쳐
#     img=io.BytesIO()
#     plt.savefig(img, format='png')
#     img_url = base64.b64encode(img.getvalue()).decode()
#     plt.close()
# #########################################################################################################################
#     avg_class=board1.objects.values('classroom').annotate(Avg('kor'), Avg('eng'), Avg('math'))


#     df = pd.DataFrame(avg_class)
#     df = df.set_index('classroom')
#     print(df)
#     df.plot(kind='bar')
#         #plt.show()#표시
#     plt.draw()#안보이게 그림을 캡쳐
#     img=io.BytesIO()
#     plt.savefig(img, format='png')
#     img_url1 = base64.b64encode(img.getvalue()).decode()
#     plt.close()


#     return render(request, 'member/graph.html', {'graph1':'data:;base64,{}'.format(img_url), 'graph2':'data:;base64,{}'.format(img_url1)})

'''
JS 차트
def js_chart(request):
    str = '100, 200, 300, 400, 300, 200, 100'
    return render(request, 'member/js_chart.html', {'str':str})
'''


# Create your views here.
